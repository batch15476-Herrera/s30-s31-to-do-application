//SECTION => dependencies and modules
const express = require("express");

//SECTION => Server Set up
    //establish a connection
const app = express();
//define a path/address
const port = 3000;

//SECTION => Database connection

//SECTION => Entry Point Response
    //bind the connection to the designated port
    app.listen(port, () => console.log(`Server running at port ${port}`));